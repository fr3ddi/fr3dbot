module.exports = function qotdClear(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (this.bot.qotd.clear(guild.id)) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.description = "Delete this server's QOTD settings";
module.exports.admin = true;
