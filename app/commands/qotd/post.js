const parseQuestion = require("./_parseQuestion");

module.exports = function qotdPost(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (this.bot.qotd.post(guild.id, ...parseQuestion(args, this.bot))) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.args = "<question>: <emoji> <answer> [emoji <answer> [emoji <answer>...]]";
module.exports.description = "Post a custom qotd";
module.exports.admin = true;
