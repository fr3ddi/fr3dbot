module.exports = function qotdTrigger(args, {channel, guild}) {
  if (!this.bot.qotd) throw "disabled";

  if (this.bot.qotd.trigger(guild.id)) {
    channel.send("Success");
  } else {
    channel.send("This server doesn't have QOTD setup.");
  }
  return true;
};

module.exports.description = "Trigger automatic QOTD now and cancel next QOTD if scheduled";
module.exports.admin = true;
