module.exports = async function updateinstagram() {
  if (!this.bot.pubsub) throw "disabled";

  await this.bot.pubsub.adapters.instagram.update();

  return true;
};

module.exports.description = "a";
module.exports.admin = true;
