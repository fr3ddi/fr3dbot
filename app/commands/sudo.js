module.exports = function sudo(args, {channel}) {
  channel.send(args.slice(1).join(" "));
  return true;
};

module.exports.args = "<text>";
module.exports.description = "Speak through the bot";
module.exports.admin = true;
