# Fr3dbot
Discord bot for Fr3ddi

---

## Requirements
- MySQL server with an account with access to a database

## Folder structure

```text
./
├─ app/   - Obsolete, will be deleted once migrated into v2
├─ cache/ - Probably also obsolete
├─ data/  - Obsolete, folder where the bot stored it's data
├─ dist/  - Transpiled code ready to be run by NodeJS
├─ src/   - Source code
│   ├── core/     - The main internals of the bot
│   │   ├── Bot/
│   │   ...   
│   ├── commands/ - Generic commands loaded at boot
│   ├── types/    - Generic argument types loaded at boot
│   ├── modules/  - Modules which provide functionality to the bot
│   ├── main.d.ts - Global type definitions
│   └── main.ts   - Entry point of the project
├── config.json - Project configuration
├── .env.sample - Example of used environment variables
└── .env        - Your own secret project configuration
```

## Modules
- Should extend abstract class `core/BotModule`
- `modules/index` exports array of modules that are instantiated at boot
- Can register their Discord commands and argument types

## WebPanel (using Socket API)
See [this repo](https://bitbucket.org/fr3ddi/fr3dbot-webpanel).
