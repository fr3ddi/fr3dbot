import { BotCommand, CommandContext, CommandReturnType } from "core/";

export default class Time extends BotCommand {
  static readonly id: string = "time";
  readonly returns = CommandReturnType.direct;

  run({ bot }: CommandContext) {
    return `My time is: ${new Date()}\n` + `Uptime: ${bot.uptime}ms`;
  }
}
