import { VoiceChannel, TextChannel, MessageEmbed } from "discord.js";
import { YtResponse } from "youtube-dl-exec";
import type { CommandContext } from "core/";
import { BotModule, CommandReturnType } from "core/";
import Cache from "utils/Cache";
import Player, { SongResponse } from "./Player";

const NOT_PLAYING_ERR = "The player is not currently playing";

export default class PlayerModule extends BotModule {
  static readonly id = "player";
  readonly name = "Player";
  readonly description = "Play music in voice chat";

  readonly commands = {
    play: {
      run: this.play,
      returns: CommandReturnType.direct,
      description: "Add a song to music queue and join your current voice channel",
      args: [{ name: "song", type: "string", infinite: true, description: "Song to add (URL or a search term)" }],
    },
    join: {
      run: this.join,
      returns: CommandReturnType.boolean,
      description: "Join your current voice channel",
      args: [{ name: "channel", type: "voiceChannel", default: null, description: "Different channel to join" }],
    },
    skip: {
      run: this.skip,
      returns: CommandReturnType.boolean,
      description: "Skip the currently playing song",
      args: [],
    },
    loop: {
      run: this.loop,
      returns: CommandReturnType.boolean,
      description: "Toggle looping of the current queue",
      args: [{ name: "state", type: "boolean", default: null, description: "Whether the queue should loop or not" }],
    },
    pause: {
      run: this.pause,
      returns: CommandReturnType.boolean,
      description: "Pause the player // resuming doesnt work well in current version",
      args: [],
    },
    resume: {
      run: this.resume,
      returns: CommandReturnType.boolean,
      description: "Resume the player // doesnt work well in current version",
      args: [],
    },
    stop: {
      run: this.stop,
      returns: CommandReturnType.boolean,
      description: "Stop playing and delete the queue",
      args: [],
    },
    updates: {
      run: this.changeUpdateChannel,
      returns: CommandReturnType.boolean,
      description: "Change the channel for song updates to the current one. Applies only to the current queue",
      args: [{ name: "channel", type: "textChannel", default: null, description: "Different channel to change to" }],
    },
    queue: {
      run: this.sendQueue,
      returns: CommandReturnType.direct,
      description: "Show the current music queue",
      args: [],
    },
  };

  private readonly songCache = new Cache<Promise<SongResponse>>(16_000, 64);
  private readonly players = new Map<string, { player: Player; updateChannel: TextChannel }>();

  private getPlayer(context: CommandContext, create = false) {
    const id = context.guild.id;
    const entry = this.players.get(id);
    if (!entry) {
      if (create) {
        const updateChannel = this.getUpdateChannel(context);

        const player = new Player(() => this.players.delete(id), this.songCache)
          .on("timeout", this.getTimeoutHandler(id))
          .on("song", this.getSongHandler(id));

        this.players.set(id, { player, updateChannel });
        return player;
      } else {
        throw NOT_PLAYING_ERR;
      }
    }

    return entry.player;
  }

  private getUpdateChannel(context: CommandContext) {
    const channel = context.message?.channel;
    if (!channel || channel.type !== "text") throw "Invalid context";
    return channel;
  }

  changeUpdateChannel(context: CommandContext, aChannel: TextChannel) {
    const entry = this.players.get(context.guild.id);
    if (!entry) throw NOT_PLAYING_ERR;
    const channel = aChannel || this.getUpdateChannel(context);
    entry.updateChannel = channel;
    return true;
  }

  skip(context: CommandContext) {
    const player = this.getPlayer(context);
    player.nextSong();
    return true;
  }

  loop(context: CommandContext, shouldLoop?: boolean) {
    const player = this.getPlayer(context);
    player.loop = shouldLoop ?? !player.loop;
    return true;
  }

  async play(context: CommandContext, ...song: string[]) {
    const player = this.getPlayer(context, true);

    const voice = context.author.voice.channel;
    if ((!player || !player.connected) && !voice) throw "Connect to a voice channel first";

    if (context.message) context.message.channel.send("Loading song(s)...");
    const [_, result] = await Promise.all([
      player.connected ? Promise.resolve() : player.changeVoice(voice),
      player.add(song.join(" ")),
    ]);
    return "Added " + ("entries" in result ? result.entries.length + " song(s)" : result.title) + " to queue";
  }

  async join(context: CommandContext, aVoice: VoiceChannel) {
    const voice = aVoice || context.author.voice.channel;
    let player: Player | undefined;
    try {
      player = this.getPlayer(context);
    } catch (err) {
      if (err !== NOT_PLAYING_ERR) throw err;
    }

    if (!voice && (!player || !player.connected)) throw "Connect to a voice channel first";
    if (!player) player = this.getPlayer(context, true);

    await player.changeVoice(voice);
    return true;
  }

  pause(context: CommandContext) {
    const player = this.getPlayer(context);
    return player.pause();
  }

  resume(context: CommandContext) {
    const player = this.getPlayer(context);
    return player.resume();
  }

  stop(context: CommandContext) {
    const player = this.getPlayer(context);
    return player.stop();
  }

  sendQueue(context: CommandContext) {
    const player = this.getPlayer(context);
    const queue = player.getQueue();

    const embed = new MessageEmbed()
      .setColor("#00afff")
      .setTitle("Current queue")
      .setDescription(`Looping: ${player.loop.toString()}`);
    if (!queue.length) embed.setDescription("is empty");
    else {
      const currentSong = player.getCurrentSong();
      if (currentSong) embed.addField("Currently playing", currentSong.title);

      // TODO: pagination
      embed.addFields(
        queue.slice(0, 15).map((song, i) => ({
          name: i + 1 + ".",
          value: song.title,
        })),
      );
    }

    return embed;
  }

  private getTimeoutHandler(id: string) {
    return () => {
      const entry = this.players.get(id);
      if (!entry) return;
      entry.updateChannel.send(
        new MessageEmbed().setColor("#00afff").setTitle("The player was idle for too long and the queue got deleted"),
      );
    };
  }

  private getSongHandler(id: string) {
    return (song: YtResponse) => {
      const entry = this.players.get(id);
      if (!entry) return;

      let description = song.description ?? "*No description*";
      if (description.length > 256) description = description.substr(0, 256) + "...";

      const embed = new MessageEmbed()
        .setColor("#00afff")
        .setTitle(song.title)
        .setDescription(description)
        .setURL(song.webpage_url || song.url);

      if (song.thumbnail) embed.setThumbnail(song.thumbnail);
      if (song.uploader) embed.setAuthor(song.uploader, undefined, song.uploader_url);

      entry.updateChannel.send("**Now playing:**", embed);
    };
  }
}
