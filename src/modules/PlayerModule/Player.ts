import EventEmitter from "events";
import { VoiceChannel, VoiceConnection, StreamDispatcher } from "discord.js";
import youtubedl, { YtResponse } from "youtube-dl-exec";
import Cache from "utils/Cache";

export type SongResponse = YtResponse | { _type: "playlist"; entries: YtResponse[] };

const MAX_QUEUE = 128;

export default class Player extends EventEmitter {
  private connection?: VoiceConnection;
  private stream?: StreamDispatcher;
  private currentSong?: Readonly<YtResponse>;
  private readonly queue: Readonly<YtResponse>[] = [];
  private timeout?: NodeJS.Timeout;
  connected = false;
  destroyed = false;
  loop = false;
  private fetching = false;

  constructor(private readonly selfDestruct: () => void, private readonly cache: Cache<Promise<SongResponse>>) {
    super();
  }

  getQueue(): Readonly<Readonly<YtResponse>[]> {
    return this.queue;
  }

  getCurrentSong() {
    return this.currentSong;
  }

  destroy() {
    if (this.destroyed) return false;
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.emit("timeout");
    }
    this.destroyed = true;
    this.stopStream();
    if (this.connection) this.connection.channel.leave();
    this.selfDestruct();
    return true;
  }

  private startTimeout(time = 30_000) {
    if (this.timeout) return;
    this.timeout = setTimeout(() => this.destroy(), time).unref();
  }
  private stopTimeout() {
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = null;
  }

  nextSong() {
    this.stopStream();
    this.currentSong = null;
    this.startStream();
  }

  pause() {
    if (!this.stream) throw "The player is not playing";
    if (this.stream.paused) throw "The player is already paused";
    this.stream.pause();
    this.startTimeout(360_000);
    return true;
  }
  resume() {
    if (!this.stream) throw "The player is not playing";
    if (!this.stream.paused) throw "The player is not paused";
    this.stream.resume();
    this.stopTimeout();
    return true;
  }

  stop() {
    return this.destroy();
  }

  private stopStream() {
    if (!this.stream) return;
    this.stream.pause();
    this.stream.destroy();
    this.stream = null;
  }

  private startStream() {
    if (this.stream && this.stream.paused) this.stream.resume();
    if (this.stream || this.destroyed || !this.connection) return;

    const song = this.currentSong || this.queue.shift();
    if (!song) {
      if (!this.fetching) this.startTimeout();
      return;
    }
    if (this.loop) this.queue.push(song);

    this.stopTimeout();

    this.currentSong = song;
    this.stream = this.connection
      .play(song.url)
      .on("start", () => {
        this.emit("song", song);
      })
      .on("finish", () => {
        this.stream = this.currentSong = null;
        setTimeout(() => this.startStream(), 1_000);
      });
  }

  async changeVoice(voice: VoiceChannel) {
    if (this.connection && this.connection.channel.id === voice.id) throw "Already connected";
    this.stopStream();
    if (this.connection) this.connection.channel.leave();

    this.connection = await voice.join();
    this.connected = true;
    this.connection.on("disconnect", () => {
      this.connected = false;
    });
    this.startStream();
  }

  async add(aUrl: string) {
    if (this.queue.length >= MAX_QUEUE) throw "The queue is full";

    if (!aUrl.startsWith("http")) {
      aUrl = "ytsearch:" + aUrl;
    }

    this.fetching = true;
    let promise = this.cache.get(aUrl);
    if (!promise) {
      promise = youtubedl(aUrl, {
        dumpSingleJson: true,
        extractAudio: true,
        audioFormat: "opus",
        noCheckCertificate: true,
        noWarnings: true,
        noMarkWatched: true,
        geoBypass: true,
        cookies: "./data/cookies.txt",
      }) as Promise<SongResponse>;
      this.cache.set(aUrl, promise);
    }
    let result: SongResponse;
    try {
      result = await promise;
    } catch (e) {
      console.error(e);
      throw "Failed to fetch the link";
    } finally {
      this.fetching = false;
    }

    if ("_type" in result) {
      result.entries.forEach((entry) => this.addSong(entry));
    } else {
      this.addSong(result);
    }
    this.startStream();

    return result;
  }

  private addSong(entry: YtResponse) {
    if (entry && "url" in entry) this.queue.push(entry);
  }
}
