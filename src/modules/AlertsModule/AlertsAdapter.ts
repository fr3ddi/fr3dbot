import type { MessageOptions } from "discord.js";

export type OnAlert = (name: string, content: string, options?: MessageOptions) => void;

export interface AlertsAdapter {
  onAlert?: OnAlert;
  subscribe(name: string): Promise<boolean>;
  unsubscribe(name: string): Promise<boolean>;
  destroy(): Promise<void>;
}

export default AlertsAdapter;
