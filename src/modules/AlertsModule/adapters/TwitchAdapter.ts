import { Util } from "discord.js";
import { ClientCredentialsAuthProvider } from "twitch-auth";
import { ApiClient } from "twitch";
import { EventSubMiddleware } from "twitch-eventsub";
import type { EventSubSubscription } from "twitch-eventsub";
import crypto from "crypto";
import http from "core/HTTPServer";
import type { AlertsAdapter, OnAlert } from "../AlertsAdapter";

const STUB = process.env.HTTP_HOST === "localhost";

export default class TwitchAdapter implements AlertsAdapter {
  private readonly twitch: ApiClient;
  private readonly eventsub: EventSubMiddleware;
  private readonly subscriptions = new Map<string, EventSubSubscription[]>();
  readonly streams = new Map<string, boolean>();

  constructor(readonly onAlert: OnAlert) {
    if (!STUB) {
      const authProvider = new ClientCredentialsAuthProvider(process.env.TWITCH_ID, process.env.TWITCH_SECRET);
      this.twitch = new ApiClient({ authProvider });

      this.eventsub = new EventSubMiddleware(this.twitch, {
        hostName: process.env.HTTP_HOST,
        pathPrefix: process.env.HTTP_PATH + "/alerts/twitch",
        secret: crypto.randomBytes(16).toString("hex"),
      });
      this.eventsub.apply(http.app);
    }
  }

  async subscribe(name: string) {
    if (STUB) return true;
    const user = await this.twitch.helix.users.getUserByName(name);
    if (!user) throw "Username not found";

    const promises = [
      this.eventsub.subscribeToStreamOnlineEvents(user, async ({ streamType }) => {
        if (streamType !== "live") return;
        this.streams.set(name, true);
        const stream = await user.getStream();
        this.onAlert(
          name,
          `${Util.escapeMarkdown(stream.userDisplayName)} is now live! **${Util.escapeMarkdown(stream.title)}**\n ` +
            `https://twitch.tv/${stream.userName}`,
        );
      }),
      this.eventsub.subscribeToStreamOfflineEvents(user, () => {
        this.streams.set(name, false);
      }),
    ];
    const subscriptions = await Promise.all(promises);
    this.subscriptions.set(name, subscriptions);
    return true;
  }

  async unsubscribe(name: string) {
    if (STUB) return true;
    const user = await this.twitch.helix.users.getUserByName(name);
    if (!user) throw "Username not found";

    const subscriptions = this.subscriptions.get(name);
    subscriptions.forEach((sub) => sub.stop());
    this.subscriptions.delete(name);
    return true;
  }

  async destroy() {
    if (STUB) return;
    await this.twitch.helix.eventSub.deleteAllSubscriptions();
  }
}
