import { Util } from "discord.js";
import YouTubeNotifier from "youtube-notification";
import crypto from "crypto";
import http from "core/HTTPServer";
import type { AlertsAdapter, OnAlert } from "../AlertsAdapter";

const STUB = process.env.HTTP_HOST === "localhost";

type NotificationData = {
  video: {
    id: string;
    title: string;
    link: string;
  };
  channel: {
    id: string;
    name: string;
    link: string;
  };
  published: string;
  updated: string;
};

export default class YouTubeAdapter implements AlertsAdapter {
  private readonly youtube: YouTubeNotifier;
  private readonly alreadyPosted: string[] = []; // ring buffer

  constructor(onAlert: OnAlert) {
    if (!STUB) {
      const path = process.env.HTTP_PATH + "/alerts/youtube";
      this.youtube = new YouTubeNotifier({
        middleware: true,
        hubCallback: "https://" + process.env.HTTP_HOST + path,
        secret: crypto.randomBytes(16).toString("hex"),
      });
      http.app.use(path, this.youtube.listener());

      this.youtube.on("notified", (data: NotificationData) => {
        if (this.alreadyPosted.includes(data.video.id)) return;
        this.alreadyPosted.push(data.video.id);
        if (this.alreadyPosted.length > 64) this.alreadyPosted.shift();

        onAlert(
          data.channel.id,
          `${Util.escapeMarkdown(data.channel.name)} has uploaded a video!\nhttps://youtu.be/${data.video.id}`,
        );
      });
    }
  }

  async subscribe(name: string) {
    if (STUB) return true;
    let listener: any;
    let failListener: any;
    return new Promise<boolean>((resolve, reject) => {
      this.youtube.once(
        "subscribe",
        (listener = ({ channel }) => {
          if (channel === name) resolve(true);
        }),
      );
      this.youtube.once(
        "denied",
        (failListener = ({ channel }) => {
          if (channel === name) reject("Denied");
        }),
      );
      this.youtube.subscribe(name);
    })
      .then(() => true)
      .finally(() => {
        this.youtube.off("subscribe", listener);
        this.youtube.off("denied", failListener);
      });
  }

  async unsubscribe(name: string) {
    if (STUB) return true;
    let listener: any;
    return new Promise<boolean>((resolve) => {
      this.youtube.once(
        "unsubscribe",
        (listener = ({ channel }) => {
          if (channel === name) resolve(true);
        }),
      );
      this.youtube.unsubscribe(name);
    })
      .then(() => true)
      .finally(() => {
        this.youtube.off("unsubscribe", listener);
      });
  }

  async destroy() {
    return;
  }
}
