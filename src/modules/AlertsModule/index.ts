import type { TextChannel, Role, MessageOptions } from "discord.js";
import type { Bot, CommandContext } from "core/";
import { BotModule, CommandReturnType } from "core/";
import http from "core/HTTPServer";
import AlertsAdapter from "./AlertsAdapter";
import * as adapters from "./adapters";

export default class AlertsModule extends BotModule {
  static readonly id = "alerts";
  readonly name = "Alerts";
  readonly description = "Alert your server of streams/videos/posts.";

  readonly commands = {
    list: {
      run: this.list,
      returns: CommandReturnType.table,
      description: "List subscribed alerts on this server",
      admin: true,
      args: [{ name: "type", type: "string", default: null, description: "Type of alerts to show, e.g. twitch" }],
    },
    types: {
      run: this.types,
      returns: CommandReturnType.direct,
      serializeResult: (result: string[]) =>
        `These alert types are available: ` + result.map((v) => "`" + v + "`").join(", "),
      description: "Get available alert types",
      admin: true,
    },
    subscribe: {
      run: this.subscribe,
      returns: CommandReturnType.boolean,
      description: "Subscribe to alerts",
      admin: true,
      args: [
        { name: "type", type: "string", description: "Type of alerts, e.g. twitch" },
        { name: "name", type: "string", description: "Name of the account to subscribe to" },
        { name: "channel", type: "textChannel", description: "Channel where to send the alerts" },
        { name: "role", type: "role", default: null, description: "Optional role to ping with alerts" },
      ],
    },
    unsubscribe: {
      run: this.unsubscribe,
      returns: CommandReturnType.boolean,
      description: "Unsubscribe from alerts",
      admin: true,
      args: [
        { name: "type", type: "string", description: "Type of alerts, e.g. twitch" },
        { name: "name", type: "string", description: "Name of the account to subscribe to" },
      ],
    },
  };

  private readonly adapters: Record<string, AlertsAdapter> = {};

  constructor(bot: Bot) {
    super(bot);
    for (const [key, Adapter] of Object.entries(adapters)) {
      this.adapters[key] = new Adapter(this.onAlert.bind(this, key));
    }

    // load subscriptions
    http.once("ready", () => {
      this.bot.db.alertsItem
        .findMany({
          distinct: ["type", "name"],
          select: { type: true, name: true },
        })
        .then((result) =>
          result.map(({ type, name }) => {
            if (!(type in this.adapters)) return;
            return this.adapters[type].subscribe(name);
          }),
        );
    });
  }

  async _destroy() {
    await Promise.all(Object.values(this.adapters).map((adapter) => adapter.destroy()));
  }

  private async onAlert(type: string, name: string, content: string, options?: MessageOptions) {
    const items = await this.bot.db.alertsItem.findMany({
      where: { type, name },
      select: { guildID: true, channelID: true, roleID: true },
    });
    if (!items || !items.length) return this.adapters[type].unsubscribe(name);
    for (const item of items) {
      const guild = this.bot.guilds.resolve(item.guildID);
      if (!guild) continue;
      const role = item.roleID && guild.roles.resolve(item.guildID);

      (guild.channels.resolve(item.channelID) as TextChannel).send(
        content + (role ? " " + role.toString() : ""),
        options,
      );
    }
  }

  async list({ guild }: CommandContext, type?: string) {
    type = type ? type.toLowerCase() : null;
    return this.bot.db.alertsItem
      .findMany({
        where: { guildID: guild.id, ...(type ? { type } : {}) },
        select: { type: true, name: true, channelID: true, roleID: true },
      })
      .then((result) =>
        result.map((item) => {
          const channel = guild.channels.resolve(item.channelID);
          const role = item.roleID && guild.roles.resolve(item.roleID);
          return {
            type: item.type,
            name: item.name,
            channel: channel ? `#${channel.name}` : null,
            role: role ? `@${role.name}` : null,
          };
        }),
      );
  }

  async types() {
    return Object.keys(this.adapters);
  }

  async subscribe(context: CommandContext, type: string, name: string, channel: TextChannel, role?: Role) {
    type = type.toLowerCase();
    if (!(type in this.adapters))
      throw `Alert type \`${type}\` is not available, you can choose one of these: ${Object.keys(adapters)
        .map((v) => "`" + v + "`")
        .join(", ")}.`;

    // If this is first of this type and name, subscribe
    // await for result in case the name cannot be subscribed to
    await this.bot.db.alertsItem
      .findFirst({
        where: { type, name },
        select: { name: true },
      })
      .then((result) => {
        if (!result) return this.adapters[type].subscribe(name);
      });

    return this.bot.db.alertsItem
      .create({
        data: {
          guild: { connect: { id: context.guild.id } },
          type,
          name,
          channelID: channel.id,
          roleID: role ? role.id : undefined,
        },
        select: { name: true },
      })
      .then(() => {
        this._updated(context.guild.id);
        return true;
      })
      .catch((err) => {
        if (err.code === "P2002") throw "Already subscribed";
        throw err;
      });
  }

  async unsubscribe(context: CommandContext, type: string, name: string) {
    type = type.toLowerCase();
    if (!(type in this.adapters))
      throw `Alert type \`${type}\` is not available, you can choose one of these: ${Object.keys(adapters)
        .map((v) => "`" + v + "`")
        .join(", ")}.`;

    return this.bot.db.alertsItem
      .delete({
        where: { guildID_type_name: { guildID: context.guild.id, type, name } },
      })
      .then(async () => {
        // If this was last one of name in type, unsubscribe globally
        this.bot.db.alertsItem
          .aggregate({
            where: { type, name },
            _count: { name: true },
          })
          .then(({ _count: { name: count } }) => {
            if (!count) return this.adapters[type].unsubscribe(name);
          });

        this._updated(context.guild.id);

        return true;
      })
      .catch(() => {
        throw "Wasn't subscribed";
      });
  }
}
