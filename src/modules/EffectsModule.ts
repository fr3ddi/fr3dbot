import { Util } from "discord.js";
import type { Message } from "discord.js";
import Uwuifier from "uwuifier";
import type { Bot, CommandContext } from "core/";
import { BotModule, CommandReturnType } from "core/";

const MAX_LEN = 1024; // max number of chars a message can have to be processed by effects

const uwuifier = new Uwuifier({
  spaces: {
    faces: 0.1,
    actions: 0.005,
    stutters: 0.25,
  },
  words: 1,
  exclamations: 0.5,
});
const transforms = {
  uwuify: uwuifier.uwuifySentence.bind(uwuifier),
};

export default class EffectsModule extends BotModule {
  static readonly id = "effects";
  readonly name = "Effects";
  readonly description = "Send a specific message when someone mentions a term";

  readonly commands = {
    add: {
      run: this.add,
      returns: CommandReturnType.boolean,
      description: "Create an effect",
      admin: true,
      args: [
        { name: "pattern", type: "string", description: "RegEx that message should match to trigger response" },
        { name: "response", type: "string", infinite: true, description: "The response of the effect" },
      ],
    },
    edit: {
      run: this.edit,
      returns: CommandReturnType.boolean,
      description: "Edit an effect",
      admin: true,
      args: [
        { name: "ID", type: "number", description: "ID of the effect" },
        { name: "pattern", type: "string", description: "RegEx that message should match to trigger response" },
        { name: "response", type: "string", infinite: true, description: "The response of the effect" },
      ],
    },
    transforms: {
      run: this.transforms,
      returns: CommandReturnType.direct,
      serializeResult: (result: string[]) => `Available transforms: ${result.map((a) => "`" + a + "`").join(", ")}`,
      description: "List available transforms",
      admin: true,
    },
    transform: {
      run: this.transform,
      returns: CommandReturnType.boolean,
      description: "Add a transform to an effect",
      admin: true,
      args: [
        { name: "ID", type: "number", description: "ID of the effect" },
        { name: "transform", type: "string", description: "Name of the transform" },
      ],
    },
    remove: {
      run: this.remove,
      returns: CommandReturnType.boolean,
      description: "Remove an effect",
      admin: true,
      args: [{ name: "ID", type: "number", infinite: true, description: "ID of the item to remove" }],
    },
    list: {
      run: this.list,
      returns: CommandReturnType.table,
      description: "List effects in this server",
      admin: true,
    },
  };

  private readonly throttle = new Map<number, Map<string, number>>();

  constructor(bot: Bot) {
    super(bot);
    bot.on("message", this.handle.bind(this));
  }

  async handle(message: Message) {
    const { guild, author, member, content, channel } = message;
    if (!guild || author.bot || !content) return;

    const _content = Util.cleanContent(content, message);
    if (_content.length > MAX_LEN) return;

    const context: CommandContext = { guild, bot: this.bot, author: member };
    const effects = await this.bot.db.effectItem.findMany({
      where: { guildID: guild.id },
      orderBy: {
        order: "asc",
      },
    });

    for (const effect of effects) {
      if (!this.throttle.has(effect.id)) this.throttle.set(effect.id, new Map());

      const match = _content.match(RegExp(effect.pattern, "i"));
      if (
        match &&
        !this.bot.shouldThrottle(context, this.throttle.get(effect.id) /* TODO: per-effect throttle settings */)
      ) {
        let response = effect.response.replace(/\{(\d+)\}/g, (_, index) => match[index]);

        if (effect.transform && transforms.hasOwnProperty(effect.transform)) {
          response = transforms[effect.transform](response);
        }

        channel.send(response);
        break; // 1 effect per message
      }
    }
  }

  async add({ guild: { id: guildID } }: CommandContext, pattern: string, ...response: string[]) {
    try {
      RegExp(pattern);
    } catch (err) {
      throw "Invalid pattern: " + err.message;
    }

    return this.bot.db.effectItem
      .create({
        data: {
          guild: { connect: { id: guildID } },
          pattern,
          response: response.join(" "),
        },
        select: { id: true },
      })
      .then(() => {
        this._updated(guildID);
        return true;
      });
  }

  async edit({ guild: { id: guildID } }: CommandContext, id: number, pattern: string, ...response: string[]) {
    return this.bot.db.effectItem
      .update({
        where: { id_guildID: { id, guildID } },
        data: {
          pattern,
          response: response.join(" "),
        },
        select: { id: true },
      })
      .then(() => {
        this._updated(guildID);
        return true;
      });
  }

  transforms() {
    return Object.keys(transforms);
  }

  async transform({ guild: { id: guildID } }: CommandContext, id: number, transform: string) {
    if (!transforms.hasOwnProperty(transform)) throw "No such transform available";

    return this.bot.db.effectItem
      .update({
        where: { id_guildID: { id, guildID } },
        data: {
          transform,
        },
        select: { id: true },
      })
      .then(() => {
        this._updated(guildID);
        return true;
      });
  }

  async remove({ guild: { id: guildID } }: CommandContext, ...id: number[]) {
    return this.bot.db.effectItem
      .deleteMany({
        where: { id: { in: id }, guildID },
      })
      .then(() => {
        this._updated(guildID);
        return true;
      });
  }

  async list({ guild }: CommandContext) {
    return this.bot.db.effectItem.findMany({
      where: { guildID: guild.id },
      select: { id: true, order: true, pattern: true, response: true },
      orderBy: { order: "asc" },
    });
  }
}
