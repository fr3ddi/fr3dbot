import { MessageEmbed } from "discord.js";
import type { MessageOptions, Message, TextChannel, Emoji, Role } from "discord.js";
import { ReactRolesItem, ReactRolesRole } from "@prisma/client";
import { Bot, BotModule, CommandContext, CommandReturnType, ArgumentError } from "core/";

const editableFields = {
  messageID: "string",
  channelID: "textChannel",
  title: "string",
  color: "color",
  description: "string",
  message: "string",
  showHelp: "boolean",
  showID: "boolean",
  showRoles: "boolean",
};

type ItemPreview = {
  id: number;
  title: string;
  color: number;
  channelName: string;
};

const ID_ARG = { name: "ID", type: "number", description: "ID of the item" }; // was duplicated too much

export default class ReactRolesModule extends BotModule {
  static readonly id = "reactroles";
  readonly name = "Reaction-roles";
  readonly description = "Allow your members to give themselves roles by reacting to a message.";

  readonly commands = {
    list: {
      run: this.list,
      returns: CommandReturnType.table,
      description: "Get all reaction-roles of the guild",
      admin: true,
    },
    get: {
      run: this.get,
      returns: CommandReturnType.direct,
      serializeResult: (result: ReactRolesItem & { roles: ReactRolesRole[] }, ctx: CommandContext) =>
        result
          ? "```yaml\n" +
            Object.entries(result)
              .filter(([field]) => field === "id" || field in editableFields)
              .map(
                ([field, value]) => `${field}: ${value}${field in editableFields ? " # " + editableFields[field] : ""}`,
              )
              .join("\n") +
            "roles:\n" +
            result.roles.map(
              (role) =>
                `- id: ${role.id}\n` +
                `  emoji: ${this.bot.resolveEmoji(role.emoji)}\n` +
                `  role: ${this.bot.resolveRole(role.role, ctx.guild)}\n` +
                `  description: ${role.description}\n`,
            ) +
            "\n```"
          : "Item not found",
      description: "Get info about a reaction-roles item",
      admin: true,
      args: [ID_ARG],
    },
    edit: {
      run: this.edit,
      returns: CommandReturnType.boolean,
      description: "Edit a react-roles item",
      admin: true,
      args: [
        ID_ARG,
        { name: "field", type: "string", description: "Name of the field to change" },
        {
          name: "value",
          type: "any",
          infinite: true,
          description: "New value of the field (must match field's data type)",
        },
      ],
    },
    create: {
      run: this.create,
      returns: CommandReturnType.direct,
      serializeResult: (id: number) => `Created new item with ID ${id}`,
      description: "Create a new reaction-roles",
      admin: true,
      args: [{ name: "channel", type: "textChannel", description: "Channel where the reaction-roles will reside" }],
    },
    destroy: {
      run: this.destroy,
      returns: CommandReturnType.boolean,
      description: "Destroy a reaction-roles item and delete it's message",
      admin: true,
      args: [{ name: "ID", type: "number", description: "ID of the item" }],
    },
    render: {
      run: this.render,
      returns: CommandReturnType.boolean,
      description: "Update or create reaction-roles item's message",
      admin: true,
      args: [ID_ARG],
    },
    addRole: {
      run: this.addRole,
      returns: CommandReturnType.boolean,
      description: "Change role on a react-roles item",
      admin: true,
      args: [ID_ARG, { name: "role", type: "role", description: "The role" }],
    },
    setRole: {
      run: this.setRole,
      returns: CommandReturnType.boolean,
      description: "Change role on a react-roles item",
      admin: true,
      args: [
        ID_ARG,
        { name: "roleID", type: "number", description: "ID of the role row" },
        { name: "role", type: "role", description: "The role" },
        { name: "emoji", type: "emoji", default: null, description: "Emoji representing the role" },
        { name: "description", type: "string", infinite: true, default: "", description: "Description fo the role" },
      ],
    },
    removeRole: {
      run: this.removeRole,
      returns: CommandReturnType.boolean,
      description: "Remove role from a react-roles item",
      admin: true,
      args: [ID_ARG, { name: "roleID", type: "number", description: "ID of the role row" }],
    },
  };

  private readonly messageIDs = new Map<string, number>();

  constructor(readonly bot: Bot) {
    super(bot);

    // Do this other way once d.js allows us to do it better
    this.bot.on("raw", async ({ t: type, d: data }) => {
      if (!type || !["MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE"].includes(type)) return;
      const { user_id, guild_id, message_id, emoji: aEmoji } = data;

      const itemID = this.messageIDs.get(message_id);
      if (!itemID || user_id === this.bot.user.id) return;

      const item = await this.bot.db.reactRolesItem.findUnique({
        where: { id_guildID: { id: itemID, guildID: guild_id } },
        include: { roles: true },
      });
      if (!item) return this.messageIDs.delete(message_id);

      const reaction = this.bot.resolveEmoji(aEmoji) || { ...aEmoji, url: "a" };

      const role = item.roles.find((_role) => {
        const emoji = _role.emoji && this.bot.resolveEmoji(_role.emoji);
        if (!emoji) return false;
        return emoji.identifier === reaction.identifier;
      });
      if (!role) {
        // Remove unexpected emoji
        return this.bot.channels
          .fetch(item.channelID)
          .then((channel: TextChannel) =>
            channel.messages
              .fetch(item.messageID)
              .then((message) => message.reactions.cache.get(reaction.url ? reaction.id : reaction.name).remove()),
          );
      }

      const guild = await this.bot.guilds.fetch(item.guildID);
      const member = await guild.members.fetch(user_id);

      if (type.endsWith("REMOVE")) return member.roles.remove(role.role);
      return member.roles.add(role.role);
    });

    this.bot.db.reactRolesItem
      .findMany({
        select: { messageID: true, id: true },
        where: { NOT: [{ messageID: undefined }] },
      })
      .then((items) => {
        items.forEach(({ messageID, id }) => this.messageIDs.set(messageID, id));
      });
  }

  async list({ guild: { id: guildID } }: CommandContext) {
    const items = await this.bot.db.reactRolesItem.findMany({
      where: { guildID },
      select: { id: true, title: true, color: true, channelID: true },
    });

    const output: ItemPreview[] = [];
    for (const item of items) {
      output.push({
        ...item,
        channelName: ((await this.bot.getChannel(item.channelID)) || {}).name,
      });
    }
    return output;
  }

  async get({ guild: { id: guildID } }: CommandContext, id: number) {
    if (!id) return null;
    return this.bot.db.reactRolesItem.findUnique({
      where: { id_guildID: { id, guildID } },
      include: { roles: true },
    });
  }

  async edit(context: CommandContext, id: number, field: string, ...value: any) {
    if (!(field in editableFields)) return false;

    // Validate and parse value
    const typeName: string = editableFields[field];
    const type = this.bot.types[typeName];
    if (!type) return false;
    value = value.length > 1 ? value.join(" ") : value[0];
    if (!type.validate(value, context)) throw new ArgumentError("value", typeName);
    value = type.parse(value, context);

    if (field === "messageID") {
      await this.bot.db.reactRolesItem
        .findUnique({
          where: { id_guildID: { id, guildID: context.guild.id } },
          select: { messageID: true, channelID: true },
        })
        .then(async ({ messageID, channelID }) => {
          if (messageID === value) return;
          this.messageIDs.delete(messageID);
          const [message] = await this.bot.getMessage(channelID, messageID);
          if (message) message.delete().catch(() => null);
        });
    }

    return this.bot.db.reactRolesItem
      .update({
        where: { id_guildID: { id, guildID: context.guild.id } },
        data: { [field]: value },
        select: { messageID: true },
      })
      .then(({ messageID }) => {
        if (messageID) this.render(context, id);
        if (field === "messageID") this.messageIDs.set(messageID, id);
        return true;
      })
      .finally(() => {
        this._updated(context.guild.id);
      });
  }

  async create({ guild: { id: guildID } }: CommandContext, { id: channelID }: TextChannel) {
    const item = await this.bot.db.reactRolesItem.create({
      data: {
        channelID,
        guild: { connect: { id: guildID } },
      },
      select: { id: true },
    });
    this._updated(guildID);
    return item.id;
  }

  async destroy(context: CommandContext, id: number) {
    const item = await this.get(context, id);
    if (!item) return null;

    if (item.channelID && item.messageID) {
      const [message] = await this.bot.getMessage(item.channelID, item.messageID);
      if (message) message.delete();
    }

    await this.bot.db.reactRolesItem.delete({
      where: { id_guildID: { id, guildID: context.guild.id } },
    });

    return true;
  }

  async conditionalRender(context: CommandContext, id: number) {
    return this.bot.db.reactRolesItem
      .findUnique({
        where: { id_guildID: { id, guildID: context.guild.id } },
        select: { messageID: true },
      })
      .then(({ messageID }) => {
        if (messageID) return this.render(context, id);
      });
  }

  async render(context: CommandContext, id: number) {
    const item = await this.get(context, id);
    if (!item) return null;

    const [_message, channel] = await this.bot.getMessage(item.channelID, item.messageID);
    let message = _message;
    if (!channel) return false;

    if (item.messageID && !message) this.messageIDs.delete(item.messageID);

    const roles = item.roles
      .filter((i) => i.emoji)
      .map((role) => ({
        ...role,
        emoji: this.bot.resolveEmoji(role.emoji),
        role: this.bot.resolveRole(role.role, context.guild),
      }));
    const embed = new MessageEmbed()
      .setTitle(item.title || "Choose your role")
      .setColor(item.color)
      .setDescription(
        (item.description ? item.description + "\n" : "") +
          (item.showHelp
            ? "React with emoji to recieve the role assigned to it, remove the reaction to lose the role.\n"
            : "") +
          (item.showHelp || item.description ? "\n" : "") +
          (item.showRoles
            ? roles
                .map(({ emoji, role, description }) => `${emoji} - ${role}` + (description ? ` - ${description}` : ""))
                .join("\n")
            : "") +
          "",
      )
      .setFooter(item.showID ? `ID: ${item.id}` : "");

    const messageData: [string, MessageOptions] = [item.message, { embed, disableMentions: "none" }];
    if (message) message.edit(...messageData);
    else {
      message = (await channel.send(...messageData)) as Message;

      await this.bot.db.reactRolesItem.update({
        where: { id_guildID: { id, guildID: context.guild.id } },
        data: { messageID: message.id },
        select: { id: true },
      });
      this.messageIDs.set(message.id, id);
      this._updated(context.guild.id);
    }

    // remove obsolete reactions
    for (const reaction of message.reactions.cache.array()) {
      if (!roles.some(({ emoji }) => emoji.identifier === reaction.emoji.identifier)) {
        reaction.remove();
      }
    }

    // add all reactions
    let reactionPromise = Promise.resolve();
    for (const { emoji } of roles) {
      reactionPromise = reactionPromise.then(() => message.react(emoji.identifier) && null);
    }

    return true;
  }

  async addRole(context: CommandContext, id: number, role: Role) {
    return this.bot.db.reactRolesRole
      .create({
        data: {
          role: role.id,
          item: { connect: { id_guildID: { id, guildID: context.guild.id } } },
        },
        select: { id: true },
      })
      .then(() => {
        this.conditionalRender(context, id);
        return true;
      })
      .finally(() => {
        this._updated(context.guild.id);
      });
  }

  async setRole(
    context: CommandContext,
    id: number,
    roleID: number,
    role: Role,
    emoji: Emoji,
    ...description: string[]
  ) {
    return this.bot.db.reactRolesRole
      .update({
        where: {
          id_itemID_guildID: { id: roleID, itemID: id, guildID: context.guild.id },
        },
        data: {
          emoji: emoji ? (emoji.url ? emoji.id : emoji.name) : undefined,
          role: role.id,
          description: description.join(" "),
        },
        select: { id: true },
      })
      .then(() => {
        this.conditionalRender(context, id);
        return true;
      })
      .finally(() => {
        this._updated(context.guild.id);
      });
  }

  async removeRole(context: CommandContext, id: number, roleID: number) {
    return this.bot.db.reactRolesRole
      .delete({
        where: {
          id_itemID_guildID: { id: roleID, itemID: id, guildID: context.guild.id },
        },
      })
      .then(() => {
        this.conditionalRender(context, id);
        return true;
      })
      .finally(() => {
        this._updated(context.guild.id);
      });
  }
}
