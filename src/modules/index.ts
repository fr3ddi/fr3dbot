import type { IBotModule } from "core/";
import AlertsModule from "./AlertsModule";
import EffectsModule from "./EffectsModule";
import PlayerModule from "./PlayerModule";
import ReactRolesModule from "./ReactRolesModule";

export default [AlertsModule, EffectsModule, PlayerModule, ReactRolesModule] as IBotModule[];
