import { Client, Emoji } from "discord.js";
import { PrismaClient } from "@prisma/client";
import { performance } from "perf_hooks";
import emojiRegex from "emoji-regex";
import type { TextChannel, Message, Guild } from "discord.js";
import type { Guild as GuildData } from "@prisma/client";

import type BotArgumentType from "./abstract/BotArgumentType";
import type { BotCommand, CommandScheme } from "./abstract/BotCommand";
import type BotModule from "./abstract/BotModule";
import type CommandContext from "./abstract/CommandContext";
import MessageHandler from "./MessageHandler";
import prepareCommand from "./prepareCommand";

import modules from "modules/";
import types from "types/";
import commands from "commands/";

export default class Bot extends Client {
  readonly isProduction = process.env.NODE_ENV === "production";
  readonly db = new PrismaClient({
    datasources: { db: { url: process.env.DATABASE_URL } },
  });
  readonly defaultThrottling = {
    global: 100,
    guild: 3000,
    channel: 2000,
    author: 10000,
  };

  readonly types: Record<string, BotArgumentType<any>> = {};
  readonly baseCommands: Record<string, BotCommand> = {};
  readonly modules: Record<string, BotModule> = {};

  readonly messageHandler: MessageHandler;

  private readonly guildDataCache = new Map<string, GuildData>();

  constructor() {
    super({
      disableMentions: "all",
      presence: {
        activity: {
          // TODO: Allow owners to modify this from web panel
          name: "queen's requests",
          type: "LISTENING",
          url: "https://twitch.tv/thedogepuppy_",
        },
      },
      ws: {
        intents: ["GUILDS", "GUILD_MEMBERS", "GUILD_MESSAGES", "GUILD_MESSAGE_REACTIONS", "GUILD_VOICE_STATES"],
      },
    });

    if (!this.isProduction) this.on("debug", console.log);
    this.on("warn", console.warn);
    this.on("error", console.error);

    this.once("ready", () => {
      console.log("Logged in as %s", this.user.tag);

      for (const guild of this.guilds.cache.values()) {
        this.onGuildCreate(guild);
      }
    });

    this.on("guildCreate", (guild) => {
      this.onGuildCreate(guild);
    });

    // Handlers
    this.messageHandler = new MessageHandler(this);

    // Load built-in types
    types.forEach((Type) => (this.types[Type.type] = new Type()));

    // Load built-in commands
    commands.forEach((Command) => (this.baseCommands[Command.id] = new Command()));

    // Load modules
    modules.forEach((Module) => (this.modules[Module.id] = new Module(this)));
  }

  async start() {
    return this.login(process.env.DISCORD_TOKEN);
  }

  async destroy() {
    await Promise.all(Object.values(this.modules).map(async (module) => await module._destroy()));
    super.destroy();
    this.db.$disconnect();
  }

  async onGuildCreate(guild: Guild) {
    const guildData = await this.db.guild.upsert({
      where: { id: guild.id },
      create: { id: guild.id, prefix: global.config.prefix },
      update: {},
    });
    this.guildDataCache.set(guild.id, guildData);
  }

  // SWR
  getGuildData(guildID: string): GuildData | Promise<GuildData> {
    const promise = this.db.guild.findUnique({ where: { id: guildID } });
    promise.then((data) => {
      this.guildDataCache.set(guildID, data);
    });

    if (this.guildDataCache.has(guildID)) return this.guildDataCache.get(guildID);
    return promise;
  }

  /**
   * Tries to find command, validates the arguments and binds them to the command ready to be called
   */
  prepareCommand(context: CommandContext, cmd: string, ...args: any[]) {
    let command: CommandScheme;
    let owner: Bot | BotModule = this; // eslint-disable-line @typescript-eslint/no-this-alias

    // Module commands
    const module = this.modules[cmd];
    if (module) {
      const subcommand = args.shift();
      command = module.commands[subcommand];
      owner = module;
    }

    // Base commands
    if (!command) command = this.baseCommands[cmd];

    // Not found
    if (!command) return null;

    // Check perms  TODO: make it better
    context.isAdmin = context.author.permissions.has("ADMINISTRATOR");
    if (command.admin && !context.isAdmin) return;

    if (!command._throttling) command._throttling = new Map();

    // Pass the duplicate to the prepare function
    return prepareCommand.apply(this, [owner, context, command, args]) as CommandScheme;
  }

  shouldThrottle(context: CommandContext, throttle: Map<string, number>, settings?: Record<string, number>) {
    const time = performance.now();
    const patchKeys: string[] = [];

    if (!settings) settings = this.defaultThrottling;

    // Check throttling
    if (
      Object.entries(settings).some(([type, cooldown]) => {
        const key = type === "global" ? "global" : type in context ? context[type].id : null;
        if (!key || !cooldown || (key !== "global" && context.isAdmin)) return; // admins can spam
        patchKeys.push(key);
        return time - (throttle.get(key) ?? 0) < cooldown;
      })
    )
      return true;

    // Update throttle times
    for (const key of patchKeys) {
      throttle.set(key, time);
    }

    return false;
  }

  resolveEmoji(str: string | { id: string; name: string }) {
    if (!str) return null;
    if (typeof str === "object") str = str.id || str.name;

    const customEmojiID = (str.match(/^(?:<:.+:)?(\d+)>?$/) || [])[1];
    if (customEmojiID) return this.emojis.resolve(customEmojiID);

    const defaultEmoji = (str.match(emojiRegex()) || [])[0];
    if (defaultEmoji) return new Emoji(this, { animated: false, name: defaultEmoji });

    return null;
  }

  resolveRole(str: string, guild: Guild) {
    if (!str) return null;
    const roleID = (str.match(/(?:<@&)?(\d+)>?/) || [])[1];
    if (roleID) return guild.roles.resolve(roleID);
    return null;
  }

  async getNumberGuilds() {
    if (this.shard) return (await this.shard.fetchClientValues("guilds.cache.size")).reduce((a, v) => a + v, 0);
    return this.guilds.cache.size;
  }

  async getChannel(channelID: string) {
    return this.channels.fetch(channelID) as Promise<TextChannel>;
  }

  async getMessage(channelID: string, messageID: string): Promise<[Message, TextChannel]> {
    let message: Message = null;
    let channel: TextChannel = null;

    if (channelID) {
      try {
        channel = await this.getChannel(channelID);

        if (messageID) {
          message = await channel.messages.fetch(messageID);
          if (message && message.deleted) message = null;
        }
      } catch (err) {
        if (!["Not Found", "Unknown Message"].includes(err.message)) throw err;
      }
    }

    return [message, channel];
  }
}
export { Bot };
