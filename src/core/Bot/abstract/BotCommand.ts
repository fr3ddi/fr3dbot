import type { CommandContext, ArgScheme } from "core/";

export enum CommandReturnType {
  void,
  boolean,
  table,
  direct,
}

export type CommandScheme = {
  description?: string;

  args?: ArgScheme[];

  admin?: boolean;

  run: (...args: any[]) => any;
  returns: CommandReturnType;
  serializeResult?: (result: any, context: CommandContext) => string;
  throttling?: {
    global: number;
    guild: number;
    channel: number;
    author: number;
  };
  _throttling?: Map<string, number>;
};

export default abstract class BotCommand implements CommandScheme {
  static readonly id: string;
  abstract readonly returns: CommandReturnType;

  abstract run(context: CommandContext, ...args: any[]): any;
}

export { BotCommand };

export interface IBotCommand {
  readonly id: string;
  new (): BotCommand;
}
