import type { Bot, CommandScheme } from "core/";

export type CommandsScheme = Record<string, CommandScheme>;

export default abstract class BotModule {
  static readonly id: string = "";
  readonly name: string = "";
  readonly description: string = "";

  readonly commands: CommandsScheme = {};

  constructor(readonly bot: Bot) {}

  _updated(guildID: string) {
    const prototype = this.constructor as typeof BotModule;
    this.bot.emit("moduleUpdate-" + guildID, prototype.id);
  }

  _destroy(): Promise<void> | void {
    return;
  }
}

export { BotModule };

export interface IBotModule {
  readonly id: string;
  new (bot: Bot): BotModule;
}
