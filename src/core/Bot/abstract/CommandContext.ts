import type { Guild, GuildMember, Message, PartialMessage } from "discord.js";
import type { Bot } from "core/";

export default interface CommandContext {
  bot: Bot;
  guild?: Guild;
  author: GuildMember;
  isAdmin?: boolean;
  message?: Message | PartialMessage;
}

export type { CommandContext };
