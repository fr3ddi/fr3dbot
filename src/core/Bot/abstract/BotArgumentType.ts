import type { CommandContext } from "core/";

export interface ArgScheme {
  name: string;
  type: string;
  description?: string;
  default?: any;
  infinite?: boolean;
}

export class ArgumentError extends Error {
  name: "ArgumentError";
  constructor(readonly argName: string, readonly type?: string, readonly position?: number) {
    super(
      type
        ? `Provided argument \`${argName}\` ` + (position ? `at ${position}th place ` : "") + `is not a valid ${type}.`
        : `Argument \`${argName}\` is missing.`,
    );
  }
}

export default abstract class BotArgumentType<Type> {
  static readonly type: string;

  validate(value: any, context: CommandContext): boolean {
    try {
      const result = this.parse(value, context);
      return result !== null && result !== undefined;
    } catch (e) {
      return false;
    }
  }

  abstract parse(value: any, context: CommandContext): Type;
}

export { BotArgumentType };

export interface IBotArgumentType {
  readonly type: string;
  new (): BotArgumentType<any>;
}
