import type { Message, PartialMessage } from "discord.js";
import textTable from "text-table";
import { Bot, CommandReturnType } from "core/";

const cmdRegex = /^(\w+)(?: (.+))?$/;
const argsRegex = RegExp(
  // oh my god there it goes
  "(?<= |^)" +
    "(?:" +
    ['"', "'", "`"]
      .map((q) => "(?<!\\\\)" + q)
      .map((q) => `${q}((?:(?!${q}).)+)${q}`)
      .join("|") +
    "|((?:\\S+(?:(?<=(?<!\\\\)\\\\)\\s)?)+)" +
    ")" +
    "(?= |$)",
  "g",
);

// NOTE: This is a module! But it has no commands... and not much in common with modules... but it could be a module, right?
export default class MessageHandler {
  constructor(private readonly bot: Bot) {
    bot.on("message", (message) => this.handle(message));
    // bot.on("messageUpdate", (_, newMessage) => this.handle(newMessage)); // NOTE: Commented out for better performance
  }

  private async handle(message: Message | PartialMessage) {
    const { member, guild, content, channel } = message;
    if (!guild || !member || this.bot.user.id === member.id) return;

    const guildData = guild && (await this.bot.getGuildData(guild.id));

    //////
    /// Command handler
    //////
    // Handles each line in a separate tick to prevent blocking event-loop
    const handleCommand = async (lines: string[], lastLine?: string) => {
      const line = lines.shift();
      if (lines.length) setImmediate(() => handleCommand(lines, line)); // Schedule nect line processing
      if (!line || line === lastLine || !line.startsWith(guildData.prefix)) return; // Check each line if it's a command and prevent repeating

      const _line = line.substr(guildData.prefix.length);
      const [_, cmd, _args] = _line.match(cmdRegex) || [];
      if (!cmd) return;
      const args = Array.from((_args || "").matchAll(argsRegex) || []).map((v) =>
        (v[1] ?? v[0]).replace(/\ /g, " ").replace(/\\+/g, "\\"),
      );

      const context = {
        bot: this.bot,
        guild,
        author: member,
        message,
      };

      try {
        const command = this.bot.prepareCommand(context, cmd, ...args);

        if (!command) return channel.send(`Command \`${line}\` not found, type \`${guildData.prefix}help\` for help`);
        const result = await command.run();

        switch (command.returns) {
          case CommandReturnType.void:
            break;

          case CommandReturnType.boolean:
            channel.send(result ? "Success" : "Command failed");
            break;

          case CommandReturnType.direct:
            channel.send("serializeResult" in command ? command.serializeResult(result, context) : result);
            break;

          case CommandReturnType.table:
            channel.send(
              result && result.length
                ? "```\n" +
                    textTable([
                      Object.keys(result[0]).map((col) => col[0].toUpperCase() + col.substr(1)),
                      ...result.map((item: Record<string, any>) => Object.values(item).map((col) => col ?? "-")),
                    ]) +
                    "```"
                : "There are no items to show.",
            );
            break;
        }
      } catch (err) {
        if (typeof err !== "string") console.error(err);
        channel.send(err.toString());
      }
    };
    handleCommand(content.split("\n"));
  }
}
