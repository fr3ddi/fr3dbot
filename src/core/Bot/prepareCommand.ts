import { Bot, BotModule, CommandScheme, CommandContext, ArgumentError } from "core/";

export default function prepareCommand(
  this: Bot,
  owner: Bot | BotModule,
  context: CommandContext,
  command: CommandScheme,
  args: any[],
) {
  //// Throttling
  if (this.shouldThrottle(context, command._throttling, command.throttling)) return null;

  //// Validate and parse arguments
  const parsedArgs = [];

  if (command.args)
    for (const arg of command.args) {
      const optional = "default" in arg;

      // Missing
      if (!args.length && !optional) throw new ArgumentError(arg.name);

      // Get BotArgumentType and the value(s)
      const type = this.types[arg.type];
      if (!type) throw `Type ${arg.type} is not loaded`;

      // Validate and parse
      const processValue = (value: any, i?: number) => {
        const loValue = String(value).toLowerCase();
        if (!optional && (!value || loValue === "null" || loValue === "none")) throw new ArgumentError(arg.name);

        // Invalid type
        if (!type.validate(value, context)) {
          // If optional and not infinite, we can skip the error
          if (optional && !arg.infinite) {
            value = null;
          } else throw new ArgumentError(arg.name, arg.type, i);
        }

        // Processing done
        parsedArgs.push(value ? type.parse(value, context) : arg.default);
      };

      // infinite argument values
      if (arg.infinite) args.forEach(processValue);
      // singular argument value
      else processValue(args.shift());
    }

  // Prepare the command to be run without arguments
  return {
    ...command,
    run: command.run.bind(owner, context, ...parsedArgs),
  } as CommandScheme;
}
