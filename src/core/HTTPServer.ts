import { EventEmitter } from "events";
import { createServer, Server } from "http";
import express from "express";
import { Server as SocketServer } from "socket.io";

export class HTTPServer extends EventEmitter {
  app: express.Application;
  server: Server;
  io: SocketServer;

  init() {
    const app = (this.app = express());
    this.server = createServer(app);
    this.io = new SocketServer(this.server, {
      serveClient: false,
      path: process.env.HTTP_PATH + "/socket",
    });

    app.set("trust proxy", true);
    // app.use(express.urlencoded({extended: true}));
    // app.use(express.json());
  }

  start() {
    return this.server.listen(Number(process.env.HTTP_PORT || 0), "localhost", () => {
      const addr = this.server.address();
      const url = typeof addr === "string" ? addr : `http://${addr.address}:${addr.port}/`;
      console.info("Listening on", url);
      this.emit("ready");
    });
  }
}

export default new HTTPServer();
