import { Socket } from "socket.io";
import crypto from "crypto";
import { Bot } from "core/";
import SocketSession from "./SocketSession";

export default function SessionManager(bot: Bot) {
  const sessions: Map<string, SocketSession> = new Map();

  return (socket: Socket) => {
    // Try to restore previous session
    let session = sessions.get(socket.handshake.query.sessionID as string);

    // Create new session
    if (!session) {
      const sessionID = crypto.randomBytes(48).toString("base64");

      socket.emit("sessionID", sessionID);

      sessions.set(sessionID, (session = new SocketSession(bot, () => sessions.delete(sessionID))));
    }

    // Bind old or new session to the new socket
    session.bindSocket(socket);
  };
}
