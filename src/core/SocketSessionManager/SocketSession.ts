import { Socket } from "socket.io";
import { Bot } from "core/";
import SharedState from "./SharedState";
import Controller from "./Controller";

const deathDelay = 4 * 60 * 60 * 1000; // Keep the session alive for 4 hours after disconnecting

export default class SocketSession {
  private dead = false;
  private deathTimeout?: NodeJS.Timeout;
  private socket?: Socket;
  readonly state: SharedState;
  readonly controller: Controller;

  private readonly listeners: Record<string, () => void> = {
    moduleUpdate: this.onModuleUpdate.bind(this),
  };

  constructor(readonly bot: Bot, private selfDestruct: () => void) {
    this.state = new SharedState(
      {
        user: {
          value: null,
          type: Object,
          editable: false,
        },
        guild: {
          value: null,
          type: String,
          editable: false,
        },
      },
      (prop: string, value: any, prevValue: any) => {
        // Emit state changes to client
        if (this.socket) this.socket.emit(toMethod("changed", prop), value);

        if (prop === "guild" && prevValue !== undefined && value !== prevValue) {
          if (prevValue) this.bot.off("moduleUpdate-" + prevValue, this.listeners.moduleUpdate);
          if (value) this.bot.on("moduleUpdate-" + value, this.listeners.moduleUpdate);
        }
      },
    );

    this.controller = new Controller(this);
  }

  onModuleUpdate(moduleID: string) {
    if (this.socket) this.socket.emit("moduleUpdate-" + moduleID);
  }

  die() {
    this.dead = true;
    if (this.socket) {
      this.socket.disconnect(true);
      this.socket = null;
    }
    this.selfDestruct();
  }

  bindSocket(socket: Socket) {
    this.socket = socket;

    // Client has reconnected
    if (this.deathTimeout) {
      // Cancel death
      clearTimeout(this.deathTimeout);
      this.deathTimeout = null;

      this.controller.do("getUser");
    }

    // Refresh data
    this.state.refresh();

    socket.on("disconnect", (reason) => {
      if (this.dead || reason === "server shutting down") return; // Don't bother

      this.socket = null;
      this.deathTimeout = setTimeout(() => this.die(), deathDelay).unref();
    });

    socket.on("logout", () => this.die());

    socket.onAny(async (eventType: string, ...args) => {
      if (this.dead || !eventType) return;
      const [type, prop] = fromMethod(eventType);
      if (!type || !prop || prop.startsWith("_")) return;

      const respond: (result: any, error?: any) => void = (() => {
        const arg = args.pop();
        // If the last arg is callback, use it
        if (typeof arg === "function") return arg;
        if (arg) args.push(arg); // put it back
        return () => null;
      })();

      switch (type) {
        case "get":
          respond(this.state.get(prop));
          break;

        case "set":
          respond(this.state.set(true, prop, args[0]));
          break;

        case "do":
          try {
            respond(await this.controller.do(prop, ...args));
          } catch (e) {
            respond(null);
            console.error(e, prop);
          }
          break;

        default:
          respond(null);
          console.error("Recevied a call to unimplemented method type '" + type + "'.");
      }
    });
  }
}

function toMethod(type: string, prop: string) {
  return type + (prop[0] || "").toUpperCase() + prop.substr(1);
}

function fromMethod(method = "") {
  const match = method.match(/^([a-z]+)([A-Z].+)$/);
  if (!match) return [];
  const [type, prop] = match.slice(1);
  return [type, (prop[0] || "").toLowerCase() + prop.substr(1)];
}
