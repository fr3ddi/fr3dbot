import { User, Permissions, Util } from "discord.js";
import crypto from "crypto";
import OAuth from "discord-oauth2";
import SocketSession from "./SocketSession";
import type { Bot, CommandContext } from "core/";
import SharedState from "./SharedState";

type GuildChannelInfo = {
  id: string;
  name: string;
  type: string;
  position: number;
};

const oauth = new OAuth({
  clientId: process.env.DISCORD_ID,
  clientSecret: process.env.DISCORD_SECRET,
});

export default class SocketController {
  readonly scope = ["identify", "guilds"];
  readonly oauth: OAuth = oauth;
  readonly bot: Bot;
  readonly state: SharedState;
  private commandContext: CommandContext;
  private readonly debounce = new Map<string, any>();

  randString: string;
  token: string;

  constructor(readonly session: SocketSession) {
    this.bot = session.bot;
    this.state = session.state;
  }

  async do(aMethod: string, ...args: any[]) {
    if (!this.bot.readyTimestamp) return null;
    const {
      groups: { moduleName, method },
    } = aMethod.match(/^(?:(?<moduleName>.+)\.)?(?<method>.+)$/);

    if (method === "constructor") return this.session.die(); // self-destruct

    // SocketController methods
    if (!moduleName && method in this && typeof this[method] === "function") {
      return await this[method](...args);
    }

    // Bot's commands
    if (!this.commandContext) throw "unauthorized";
    const command = this.bot.prepareCommand(
      this.commandContext,
      moduleName || method,
      ...(moduleName ? [method] : []),
      ...args,
    );

    if (command) {
      const result = await command.run();
      this.debounce.set(aMethod, result);
      return result;
    }

    // "Bypass" rate limiting
    return this.debounce.get(aMethod);
  }

  async getBot() {
    const user: User = this.bot.user;
    return {
      id: user.id,
      avatar: user.avatar,
      tag: user.tag,
      readyTimestamp: this.bot.readyTimestamp,
      guilds: await this.bot.getNumberGuilds(),
      production: this.bot.isProduction,
      modules: Object.keys(this.bot.modules),
    };
  }

  getModules() {
    const guild = this.state.get("guild");
    if (!guild) return null;

    return Object.entries(this.bot.modules).map(([id, module]) => ({
      id,
      name: module.name,
      description: module.description,
    }));
  }

  getAllGuildEmojis() {
    return Array.from(this.bot.guilds.cache.values()).map((guild) => ({
      id: guild.id,
      name: guild.name,
      icon: guild.icon,
      emojis: Array.from(guild.emojis.cache.values()).map((emoji) => ({
        name: emoji.name,
        id: emoji.id,
        url: emoji.url,
        guild: guild.id,
      })),
    }));
  }

  async getRoles() {
    const guildID = this.state.get("guild");
    if (!guildID) return null;
    const guild = await this.bot.guilds.fetch(guildID);

    return await guild.roles.fetch().then((roles) =>
      Array.from(Util.discordSort(roles.cache).values())
        .filter((role) => !role.managed && role.name !== "@everyone")
        .map((role) => ({
          id: role.id,
          color: role.color,
          name: role.name,
        })),
    );
  }

  async getChannels(type?: string | string[]): Promise<GuildChannelInfo[]> {
    const guildID = this.state.get("guild");
    if (!guildID) return null;
    const guild = await this.bot.guilds.fetch(guildID);

    const channels = [];
    for (const channel of Util.discordSort(guild.channels.cache).values()) {
      if (channel.type === "category") continue;
      if (type && (Array.isArray(type) ? !type.includes(channel.type) : channel.type !== type)) continue;

      channels.push({
        id: channel.id,
        name: channel.name,
        type: channel.type,
        position: channel.calculatedPosition,
      });
    }

    return channels;
  }

  async _handleAuth({ access_token, refresh_token }: { access_token: string; refresh_token: string }) {
    this.token = access_token;

    this.getUser();

    return encryptToken(refresh_token);
  }

  preauth(redirectUri: string) {
    if (this.token) return null;

    if (!this.randString) this.randString = crypto.randomBytes(16).toString("hex");

    return {
      authUrl: this.oauth.generateAuthUrl({
        redirectUri,
        scope: this.scope,
        state: this.randString,
        prompt: "none",
      }),
    };
  }

  async auth(code: string, state: string, redirectUri: string) {
    if (state !== this.randString) return null;
    this.randString = null; // Reset randstring to prevent re-use of auth code

    return this.oauth
      .tokenRequest({
        code,
        redirectUri,
        scope: this.scope,
        grantType: "authorization_code",
      })
      .then(this._handleAuth.bind(this));
  }

  async refresh(tokenCipher: string) {
    return this.oauth
      .tokenRequest({
        refreshToken: await decryptToken(tokenCipher),
        scope: this.scope,
        grantType: "refresh_token",
      })
      .then(this._handleAuth.bind(this));
  }

  async getUser() {
    if (!this.token) return false;
    try {
      // TODO: May this be spedup by fetching guilds over websockets?
      // TODO: Anyway caching user might be useful
      const [user, guilds] = await Promise.all([
        this.state.get("user") || this.oauth.getUser(this.token),
        this.oauth.getUserGuilds(this.token),
      ]);

      // Find managable guilds
      user.guilds = {};

      for (const guild of guilds) {
        if (!new Permissions(guild.permissions).has("ADMINISTRATOR")) continue;
        try {
          const guildData = this.bot.guilds.resolve(guild.id); // .resolve instead of .fetch cuz d.js caches guilds on boot
          if (!guildData.available) continue;
        } catch (_) {
          continue;
        }

        user.guilds[guild.id] = guild;
      }

      // Reselect guild to be sure
      const selectedGuild = this.state.get("guild");
      if (selectedGuild) this.selectGuild(selectedGuild);

      this.state.set(false, "user", user);
    } catch (_) {
      return false;
    }
    return true;
  }

  async selectGuild(guildID: string) {
    const user = this.state.get("user");
    if (!user || (guildID !== null && (!user.guilds || !(guildID in user.guilds)))) return false;

    // (Re)generate context
    const guild = this.bot.guilds.resolve(guildID);
    this.commandContext = {
      bot: this.bot,
      guild,
      isAdmin: true,
      author: await guild.members.fetch(user.id),
    };

    return this.state.set(false, "guild", guildID);
  }
}

function encryptToken(token: string): Promise<string> {
  return new Promise((resolve, reject) => {
    // Generate salt
    const salt = crypto.randomBytes(16).toString("hex");

    // Generate key
    crypto.scrypt(process.env.CRYPTO_SECRET, salt, 24, (err, key) => {
      if (err) return reject(err);

      // Generate inicialization vector
      crypto.randomFill(new Uint8Array(16), (err2, iv) => {
        if (err2) return reject(err2);

        try {
          const cipher = crypto.createCipheriv(process.env.CRYPTO_ALG, key, iv);

          const encryptedPart = cipher.update(token, "utf-8", "hex");

          const data = {
            salt,
            iv: Array.from(iv),
            encrypted: encryptedPart + cipher.final("hex"),
          };
          resolve(Buffer.from(JSON.stringify(data)).toString("base64"));
        } catch (e) {
          return reject(e);
        }
      });
    });
  });
}

function decryptToken(tokenCipher: string): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      const { iv, salt, encrypted } = JSON.parse(Buffer.from(tokenCipher, "base64").toString());

      // Generate key
      crypto.scrypt(process.env.CRYPTO_SECRET, salt, 24, (err, key) => {
        if (err) return reject(err);

        try {
          const decipher = crypto.createDecipheriv(process.env.CRYPTO_ALG, key, Uint8Array.from(iv));

          const decryptedPart = decipher.update(encrypted, "hex", "utf-8");

          resolve(decryptedPart + decipher.final("utf-8"));
        } catch (e) {
          return reject(e);
        }
      });
    } catch (e) {
      return reject(e);
    }
  });
}
