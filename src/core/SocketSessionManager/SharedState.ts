export default class SharedState {
  state: Record<string, any> = {};

  constructor(
    state?: Record<string, any>,
    private onChange: (prop: string, value: any, prevValue?: any) => void = () => null,
  ) {
    if (state) this.define(state);
  }

  refresh() {
    for (const [prop, { value }] of Object.entries(this.state)) {
      this.onChange(prop, value);
    }
  }

  load(state: Record<string, any> = {}) {
    for (const [prop, value] of Object.entries(state)) {
      this.set(false, prop, value);
    }
  }

  define(state: Record<string, any> = {}) {
    for (const [prop, propData] of Object.entries(state)) {
      this.create(prop, propData.value, propData.type, propData.editable);
    }
  }

  create(prop: string, value = null, type = null, editable = true) {
    this.state[prop] = { type, value, editable };
  }

  set(external: boolean, prop: string, value: any) {
    if (!(prop in this.state)) return null;

    const state = this.state[prop];

    if (external && !state.editable) return false;
    if (state.type && value !== null && typeof value !== typeof state.type()) {
      return false;
    }

    const prevValue = this.state[prop].value;
    this.state[prop].value = value;

    this.onChange(prop, value, prevValue);
    return true;
  }

  get(prop: string) {
    if (!(prop in this.state)) return null;
    return this.state[prop].value;
  }
}
