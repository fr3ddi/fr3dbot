import { Snowflake } from "discord.js";

declare global {
  interface Config {
    readonly prefix: string;
    readonly owner: Snowflake[];
    readonly cachePath: string;
    readonly socketController: boolean;
  }

  namespace NodeJS {
    interface Global {
      config: Config;
    }
  }
}
