import { config as dotenv } from "dotenv";
import Bot from "core/Bot";
import http from "core/HTTPServer";
import SocketSessionManager from "core/SocketSessionManager";

// Do not compile config.json; changes resulting
const config: Config = require("../config.json"); // tslint:disable-line

dotenv();
if (!process.env.DISCORD_TOKEN) throw Error("Missing the environment variable DISCORD_TOKEN");
global.config = config;

// Init
http.init();
const bot = new Bot();

const killHandler = async () => {
  console.log("Logging out...");
  await bot.destroy();
  process.exit(1);
};

bot.once("ready", () => {
  console.log("Ready");
  process.on("SIGINT", killHandler);
  process.on("SIGTERM", killHandler);
});

// Start
bot.start().catch((err) => console.error("Boot failed", err));

if (config.socketController) http.io.on("connection", SocketSessionManager(bot));

http.start();
