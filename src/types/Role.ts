import type { Role } from "discord.js";
import { BotArgumentType, CommandContext } from "core/";

export default class RoleArgumentType extends BotArgumentType<Role> {
  static readonly type = "role";

  parse(value: any, { bot, guild }: CommandContext) {
    return bot.resolveRole(value, guild);
  }
}
