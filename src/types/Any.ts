import { BotArgumentType } from "core/";

export default class AnyArgumentType extends BotArgumentType<boolean> {
  static readonly type = "any";

  parse(value: any) {
    return value;
  }
}
