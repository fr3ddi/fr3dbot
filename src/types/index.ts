import { IBotArgumentType } from "core/";
import AnyType from "./Any";
import BooleanType from "./Boolean";
import ColorType from "./Color";
import EmojiType from "./Emoji";
import NumberType from "./Number";
import RoleType from "./Role";
import StringType from "./String";
import TextChannelType from "./TextChannel";
import VoiceChannelType from "./VoiceChannel";

export default [
  AnyType,
  BooleanType,
  ColorType,
  EmojiType,
  NumberType,
  RoleType,
  StringType,
  TextChannelType,
  VoiceChannelType,
] as IBotArgumentType[];
