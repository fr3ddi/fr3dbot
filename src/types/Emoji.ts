import type { Emoji } from "discord.js";
import { BotArgumentType, CommandContext } from "core/";

export default class EmojiArgumentType extends BotArgumentType<Emoji> {
  static readonly type = "emoji";

  parse(value: any, { bot }: CommandContext) {
    return bot.resolveEmoji(value);
  }
}
