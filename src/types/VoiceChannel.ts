import type { VoiceChannel } from "discord.js";
import { BotArgumentType, CommandContext } from "core/";

export default class VoiceChannelArgumentType extends BotArgumentType<VoiceChannel> {
  static readonly type = "voiceChannel";

  parse(value: any, { guild }: CommandContext) {
    const channel = guild.channels.resolve(value.replace(/^<.(.+)>$/, "$1"));
    if (channel.type === "voice") return channel as VoiceChannel;
  }
}
