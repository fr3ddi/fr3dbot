import { BotArgumentType } from "core/";

export default class FloatArgumentType extends BotArgumentType<number> {
  static readonly type = "number";

  parse(value: any) {
    return Number(value);
  }
}
