import type { TextChannel } from "discord.js";
import { BotArgumentType, CommandContext } from "core/";

export default class TextChannelArgumentType extends BotArgumentType<TextChannel> {
  static readonly type = "textChannel";

  parse(value: any, { guild }: CommandContext) {
    const channel = guild.channels.resolve(value.replace(/^<.(.+)>$/, "$1"));
    if (channel.isText) return channel as TextChannel;
  }
}
