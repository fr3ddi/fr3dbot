import { BotArgumentType } from "core/";

export default class StringArgumentType extends BotArgumentType<string> {
  static readonly type = "string";

  parse(value: any) {
    return String(value);
  }
}
