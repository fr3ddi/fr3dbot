import { BotArgumentType } from "core/";

export default class BooleanArgumentType extends BotArgumentType<boolean> {
  static readonly type = "boolean";

  parse(value: any) {
    if (String(value).match(/^(y(e(s|ah?)?|up)?)|1|t(rue)?$/i)) return true;
    if (String(value).match(/^(n(o(pe?)?)?)|0|f(alse)?$/i)) return false;
  }
}
