import Color from "color";
import { BotArgumentType } from "core/";

export default class ColorArgumentType extends BotArgumentType<number> {
  static readonly type = "color";

  parse(value: any) {
    return Color(value.toLowerCase()).rgbNumber();
  }
}
